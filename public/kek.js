var slideWidth=255; 
var chetnost; 
if($(window).width()>1000) chetnost=4; else  chetnost=2;
$(window).on('resize', function(){ 
var win = $(this); //this = window 
if (win.width() < 1000) { chetnost=2; $(".dot.si").removeClass("ki"); }
else { chetnost=4; $(".dot.si").addClass("ki"); } 
}); 

$(function(){ 

$(".slidewrapper").width($(".slidewrapper").children().size()*(4/chetnost)*(slideWidth*chetnost)); 
$("#next_slide").click(function(){nextSlide();}); 
$("#prev_slide").click(function(){prevSlide();}); 
$(".dot").click(function(){ 
$(".dot.active").removeClass("active"); 
$(this).addClass("active"); 
var n=$(".dot").index(this); 
certainSlide(n); 
}); 
if(chetnost==2) $(".dot.ki").removeClass("ki"); 
}); 

function nextSlide(){ 
var currentSlide=parseInt($(".slidewrapper").data("current")); 
currentSlide++; 
if(currentSlide>=$(".slidewrapper").children().size()*(2/chetnost)) 
{ 
currentSlide=0; 
} 
$(".dot.active").removeClass("active"); 
$(".dot").eq(currentSlide).addClass("active"); 
$(".slidewrapper").animate({left: -currentSlide*(slideWidth*chetnost)},1000).data("current",currentSlide); 
} 

function prevSlide(){ 
var currentSlide=parseInt($(".slidewrapper").data("current")); 
currentSlide--; 
if(currentSlide<0) 
{ 
currentSlide=$(".slidewrapper").children().size()*(2/chetnost)-1; 
} 
$(".dot.active").removeClass("active"); 
$(".dot").eq(currentSlide).addClass("active"); 
$(".slidewrapper").animate({left: -currentSlide*(slideWidth*chetnost)},1000).data("current",currentSlide); 
} 

function certainSlide(n){ 
var currentSlide=n; 
$(".slidewrapper").animate({left: -currentSlide*(slideWidth*chetnost)},1000).data("current",currentSlide); 
}